#!/usr/bin/env ruby

require "optparse"

class GaussCount
	# Create an accessor
	attr_accessor :number

	# create the object
	def initialize(number = 100)
		@number = number
	end

	# addup digits
	def digit_add(number)
		if number.nil?
			puts "Empty number"
		else
			spec = 0
			number.to_s.scan(/./).each do |i|
				spec += i.to_i
			end
			return spec
		end
	end


	# calculate and return all pair addition
	def calculate_pair
		if @number.nil?
			puts "No number was given"
		else
			pairs = @number / 2
			single = @number + 1
			puts "#{@number} => #{pairs * single}"
		end
	end

	# calculate and return all digit addition
	def calculate_digit
		if @number.nil?
			puts "No number was given"
		else
			pairs = @number / 2
			single = digit_add(@number - 1)
			last = digit_add(@number)
			puts "#{@number} => #{(pairs * single) + last}"
		end
	end


	if __FILE__ == $0
		options = {}
		OptionParser.new do |opts|
			opts.banner = "Usage: __FILE__ [flags] number"

			opts.on("-d", "--digit", "Use digit addition") do |d|
				options[:digit] = d
			end
		end.parse!

		if ARGV.empty?
			abort("No argument given!")
		else
			gauss = GaussCount.new(ARGV.first.to_i)
			if options[:digit]
				gauss.calculate_digit
			else
				gauss.calculate_pair
			end
		end
	end
end
